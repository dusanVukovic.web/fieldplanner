package com.dusan.fieldPlanner.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dusan.fieldPlanner.dao.FieldRepository;
import com.dusan.fieldPlanner.entity.Field;

@Service
public class FiledServiceImpl implements FieldService {

	private FieldRepository fieldRepository;
	
	@Autowired
	public FiledServiceImpl(FieldRepository theFieldRepository) {
		this.fieldRepository = theFieldRepository;
	}

	@Override
	public List<Field> findAll() {
		return fieldRepository.findAll();
	}

}
