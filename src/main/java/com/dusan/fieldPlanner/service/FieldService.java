package com.dusan.fieldPlanner.service;

import java.util.List;

import com.dusan.fieldPlanner.entity.Field;

public interface FieldService {
	public List<Field> findAll();
}
