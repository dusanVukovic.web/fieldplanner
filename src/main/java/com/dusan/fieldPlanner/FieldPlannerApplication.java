package com.dusan.fieldPlanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FieldPlannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FieldPlannerApplication.class, args);
	}

}
