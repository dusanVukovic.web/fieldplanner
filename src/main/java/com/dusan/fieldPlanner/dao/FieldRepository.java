package com.dusan.fieldPlanner.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dusan.fieldPlanner.entity.Field;

@Transactional
public interface FieldRepository extends JpaRepository<Field, Integer> {
}
