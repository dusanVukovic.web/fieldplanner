package com.dusan.fieldPlanner.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dusan.fieldPlanner.entity.Field;
import com.dusan.fieldPlanner.service.FieldService;

@RestController
@RequestMapping("/api")
public class FieldController {

	private FieldService fieldService;
	
	public FieldController(FieldService theFieldService) {
		this.fieldService = theFieldService;
	}
	
	@GetMapping("/fields")
	public List<Field> findAll() {
		return fieldService.findAll();
	}

}
